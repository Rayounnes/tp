package com.example.e1643891.tpmobile.DAL;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mlevasseur on 2018-10-14.
 */

public class Programme implements Serializable {

    @SerializedName("Id")
    private int id;
    @SerializedName("Nom")
    private String nom;
    @SerializedName("Duree")
    private int duree;

    @SerializedName("Competences")
    private List<Competence> competences = new ArrayList<>();
    @SerializedName("ChampsCompetences")
    private List<ChampCompetence> champsCompetences;

    public Programme() {}

    public Programme(int id) {
        this.id = id;
    }

    public Programme(int id, String nom, int duree) {
        this.id = id;
        this.nom = nom;
        this.duree = duree;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public List<Competence> getCompetences() {
        return competences;
    }

    public List<ChampCompetence> getChampsCompetences() {
        return champsCompetences;
    }

    @Override
    public boolean equals(Object o) {

        boolean egale = false;

        if (o != null) {
            if (getClass() == o.getClass()) {

                Programme c = (Programme) o;
                egale = (id == c.id);
            }
        }
        return egale;
    }

    @Override
    public int hashCode() {
        int result = id;
        return result;
    }
    @Override
    public String toString() {
        return nom;
    }
}
