package com.example.e1643891.tpmobile.DAL;

/**
 * Created by mlevasseur on 2018-10-14.
 */

public class SavoirCompetence {
      
        public int Id;
        public String Description;
        public int Type;     
        public int No ;    
        public int IdCompetence;     
        public Competence Competence;
		
		  public SavoirCompetence() {}

    public SavoirCompetence(int id) {
        this.Id = id;
    }


    public SavoirCompetence(int id, String Description, int type, int no, int idComp, Competence Comp) {
        this.Id = id;
        this.Description = Description;
        this.Type = type;
        this.No = no;
        this.IdCompetence = idComp;
        this.Competence = Comp;
    }
	
	 public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }
	
	 public String getDescription() {
        return Description;
    }

    public void setDescription(String desc) {
        this.Description = desc;
    }
	
	 public int getType() {
        return Type;
    }

    public void setType(int type) {
        this.Type = type;
    }
	
	 public int getNo() {
        return No;
    }

    public void setNo(int no) {
        this.No = no;
    }
	
	 public int getIdCompetence() {
        return IdCompetence;
    }

    public void setIdCompetence(int id) {
        this.IdCompetence = id;
    }
	
	 public Competence getCompetence() {
        return Competence;
    }

    public void setCompetence(Competence comp) {
        this.Competence = comp;
    }
	
	
	  @Override
    public boolean equals(Object o) {

        boolean egale = false;

        if (o != null) {
            if (getClass() == o.getClass()) {

                SavoirCompetence s = (SavoirCompetence) o;
                egale = (Id == s.Id);
            }
        }
        return egale;
    }

    @Override
    public int hashCode() {
        int result = Id;
        result = 31 * result + (Description != null ? Description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return this.No + ". " + this.getDescription();
    }
	
	//Je ne connais pas assez bien les parcelable pour l'implanter, Tuan je te laisse le faire
	
	
	
	
	
	
	
	
	
}
