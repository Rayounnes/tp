package com.example.e1643891.tpmobile.View;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.e1643891.tpmobile.API.Api;
import com.example.e1643891.tpmobile.API.ListCoursApi;
import com.example.e1643891.tpmobile.DAL.ChampCompetence;
import com.example.e1643891.tpmobile.DAL.Cours;
import com.example.e1643891.tpmobile.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CoursFragment extends Fragment {

    private Cours cours;
    private Context context;
    private List<ChampCompetence> cc;
    private Spinner spin_ChampComp;
    private View v;

    TextView txt_Sigle;
    TextView txt_Titre;
    TextView txt_Descrription;
    TextView txt_Contenu;
    TextView txt_Duree;
    TextView txt_Ponderation;
    TextView txt_Epreuve;

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Api.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    Api api = retrofit.create(Api.class);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cours = new Cours();
        context = getActivity().getApplicationContext();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_cours,
                container, false);

        //Ramener le cours
        Intent intent = getActivity().getIntent();
        cours =(Cours) intent.getSerializableExtra("cours");

        //Set up le OnClick pour le bouton save
        Button btn = v.findViewById(R.id.Btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifierCours(cours.getId());
                getActivity().onBackPressed();
            }
        });

        //Peupler les champs
        getChampsCompetences(cours.getId());
        txt_Sigle = v.findViewById(R.id.txt_Sigle);
        txt_Titre = v.findViewById(R.id.txt_Titre);
        txt_Descrription = v.findViewById(R.id.txt_Descrription);
        txt_Contenu = v.findViewById(R.id.txt_Contenu);
        txt_Duree = v.findViewById(R.id.txt_Duree);
        txt_Ponderation = v.findViewById(R.id.txt_Ponderation);
        txt_Epreuve = v.findViewById(R.id.txt_epreuveFinale);

        txt_Sigle.setText(cours.getSigle());
        txt_Titre.setText(cours.getTitre());
        txt_Descrription.setText(cours.getDescription());
        txt_Contenu.setText(cours.getContenu());
        txt_Epreuve.setText(cours.getEpreuveFinale());
        txt_Duree.setText(String.valueOf(cours.getDuree()));
        txt_Ponderation.setText(cours.getPonderation());

        return v;
    }

    public void getChampsCompetences(int idCours) {
        Call<List<ChampCompetence>> call = api.getChampsCompetencesParCours(idCours);

        call.enqueue(new Callback<List<ChampCompetence>>() {
            @Override
            public void onResponse(Call<List<ChampCompetence>> call, Response<List<ChampCompetence>> response) {
                cc = response.body();
                spin_ChampComp = v.findViewById(R.id.Spin_ChampComp);
                ArrayAdapter<ChampCompetence> adapter = new ArrayAdapter<ChampCompetence>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item , cc );
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spin_ChampComp.setAdapter(adapter);
                for(ChampCompetence c : cc){
                    if(c.getId() == cours.getIdChampCompetence()){
                        spin_ChampComp.setSelection(cc.indexOf(c));
                    }
                }

            }
            @Override
            public void onFailure(Call<List<ChampCompetence>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void modifierCours(int idCours) {


        String sigle = txt_Sigle.getText().toString();
        String titre = txt_Titre.getText().toString();
        String desc = txt_Descrription.getText().toString();
        String contenu = txt_Contenu.getText().toString();
        int duree = Integer.parseInt(txt_Duree.getText().toString());
        String ponderation = txt_Ponderation.getText().toString();
        String epreuve = txt_Epreuve.getText().toString();
        ChampCompetence champ = (ChampCompetence)  spin_ChampComp.getSelectedItem();
        Cours cour = new Cours(idCours,sigle,titre,desc,duree,contenu,ponderation,epreuve,champ.getId());

        Call<Cours> call = api.putCours(idCours,cour);
        call.enqueue(new Callback<Cours>() {
            @Override
            public void onResponse(Call<Cours> call, Response<Cours> response) {
                Cours cours = response.body();
            }
            @Override
            public void onFailure(Call<Cours> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

}
