package com.example.e1643891.tpmobile.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.example.e1643891.tpmobile.DAL.Cours;
import com.example.e1643891.tpmobile.R;

public class CoursActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getSupportFragmentManager();
        Fragment frag_Container = fm.findFragmentById(R.id.fragment_container);


        if (frag_Container == null) {
            frag_Container = new CoursFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, frag_Container)
                    .commit();


        }
    }
}
