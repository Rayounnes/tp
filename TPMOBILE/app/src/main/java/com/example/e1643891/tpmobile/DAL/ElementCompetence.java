package com.example.e1643891.tpmobile.DAL;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by mlevasseur on 2018-10-14.
 */

public class ElementCompetence {

    @SerializedName("Id")
    private int id;
    @SerializedName("No")
    private int no;
    @SerializedName("Element")
    private String element;
    @SerializedName("IdCompetence")
    private int idCompetence;
    @SerializedName("Competence")
    private Competence competence;
    @SerializedName("CriterePerformances")
    private List<CriterePerformance> criterePerformances = new ArrayList<>();
    @SerializedName("Savoirs")
    private List<Savoir> savoirs;

    public ElementCompetence() {}

    public ElementCompetence(int id) {
        this.id = id;
    }

    public ElementCompetence(int id, int no, String element, int idCompetence) {
        this.id = id;
        this.no = no;
        this.element = element;
        this.idCompetence = idCompetence;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public int getIdCompetence() {
        return idCompetence;
    }

    public void setIdCompetence(int idCompetence) {
        this.idCompetence = idCompetence;
    }

    public Competence getCompetence() {
        return competence;
    }

    public void setCompetence(Competence competence) {
        this.competence = competence;
    }

    public Collection<CriterePerformance> getCriterePerformances() {
        return criterePerformances;
    }

    public Collection<Savoir> getSavoirs() {
        return savoirs;
    }

    public void setSavoirs(List<Savoir> savoirs) {
        this.savoirs = savoirs;
    }

    @Override
    public boolean equals(Object o) {

        boolean egale = false;

        if (o != null) {
            if (getClass() == o.getClass()) {

                ElementCompetence c = (ElementCompetence) o;
                egale = (id == c.id);
            }
        }
        return egale;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + no;
        result = 31 * result + (element != null ? element.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return this.getElement();
    }
}
