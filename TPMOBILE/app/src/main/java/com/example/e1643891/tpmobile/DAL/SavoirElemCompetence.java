package com.example.e1643891.tpmobile.DAL;

import com.google.gson.annotations.SerializedName;

public class SavoirElemCompetence {

    @SerializedName("Id")
    public int id;
    @SerializedName("Description")
    public String description;
    @SerializedName("Type")
    public int type;
    @SerializedName("No")
    public int no;
    @SerializedName("IdElementCompetence")
    public int idelemcomp;

    public SavoirElemCompetence(int id, String description, int type, int no, int idelemcomp) {
        this.id = id;
        this.description = description;
        this.type = type;
        this.no = no;
        this.idelemcomp = idelemcomp;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public int getType() {
        return type;
    }

    public int getNo() {
        return no;
    }

    public int getIdelemcomp() {
        return idelemcomp;
    }

    @Override
    public String toString() {
        return this.getDescription();
    }
}

