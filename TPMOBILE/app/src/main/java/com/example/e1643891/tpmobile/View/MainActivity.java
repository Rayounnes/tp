package com.example.e1643891.tpmobile.View;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.e1643891.tpmobile.R;

public class MainActivity extends AppCompatActivity {

    FragmentManager fm;
    Fragment frag_Container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null) {
            fm = getSupportFragmentManager();
            frag_Container = fm.findFragmentById(R.id.fragment_container);

            if (frag_Container == null) {
                frag_Container = new ListCoursFragment();
                fm.beginTransaction()
                        .add(R.id.fragment_container, frag_Container)
                        .commit();
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        frag_Container = ListCoursFragment.newInstance();
        fm.beginTransaction()
                .add(R.id.fragment_container, frag_Container)
                .commit();
    }
}
