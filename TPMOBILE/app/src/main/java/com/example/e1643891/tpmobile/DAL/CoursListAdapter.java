package com.example.e1643891.tpmobile.DAL;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.e1643891.tpmobile.R;
import java.util.List;

public class CoursListAdapter extends ArrayAdapter<Cours> {

    private Context context;
    private List<Cours> cours;

    public CoursListAdapter(Context context, int resource, List<Cours> cours) {
        super(context, resource, cours);
        this.context = context;
        this.cours = cours;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listCours = convertView;

        if(listCours == null)
            listCours = LayoutInflater.from(this.context).inflate(R.layout.list_string,parent,false);

        Cours cour = cours.get(position);
        TextView text = listCours.findViewById(R.id.textView);
        TextView text2 = listCours.findViewById(R.id.textView2);
        text.setText(cour.getSigle());
        text2.setText(cour.getTitre());




        return listCours;
    }
}
