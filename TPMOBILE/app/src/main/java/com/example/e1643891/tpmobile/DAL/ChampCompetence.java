package com.example.e1643891.tpmobile.DAL;

import com.google.gson.annotations.SerializedName;

public class ChampCompetence {

    @SerializedName("Id")
    private int Id;

    @SerializedName("Enonce")
    private String enonce;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getEnonce() {
        return enonce;
    }

    public void setEnonce(String enonce) {
        this.enonce = enonce;
    }

    @Override
    public String toString() {
        return this.getEnonce();            // What to display in the Spinner list.
    }
}
