package com.example.e1643891.tpmobile.DAL;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Cours implements Serializable {
    @SerializedName("Id")
    private int Id;
    @SerializedName("Sigle")
    private String Sigle;
    @SerializedName("Titre")
    private String Titre;
    @SerializedName("Description")
    private String Description;
    @SerializedName("Duree")
    private int Duree;
    @SerializedName("Contenu")
    private String Contenu;
    @SerializedName("Ponderation")
    private String Ponderation;
    @SerializedName("EpreuveFinale")
    private String EpreuveFinale;
    @SerializedName("IdChampCompetences")
    private int IdChampCompetence;
    @SerializedName("ChampCompetence")
    private ChampCompetence ChampCompetence;


    public Cours(){}

    public Cours(int id, String sigle, String titre, String description, int duree, String contenu, String ponderation, String epreuveFinale, int idChampCompetence) {
        this.Id = id;
        this.Sigle = sigle;
        this.Titre = titre;
        this.Description = description;
        this.Duree = duree;
        this.Contenu = contenu;
        this.Ponderation = ponderation;
        this.EpreuveFinale = epreuveFinale;
        this.IdChampCompetence = idChampCompetence;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public String getSigle() {
        return Sigle;
    }

    public void setSigle(String sigle) {
        Sigle = sigle;
    }

    public String getTitre() {
        return Titre;
    }

    public void setTitre(String titre) {
        Titre = titre;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getDuree() {
        return Duree;
    }

    public void setDuree(int duree) {
        Duree = duree;
    }

    public String getContenu() {
        return Contenu;
    }

    public void setContenu(String contenu) {
        Contenu = contenu;
    }

    public String getPonderation() {
        return Ponderation;
    }

    public void setPonderation(String ponderation) {
        Ponderation = ponderation;
    }

    public String getEpreuveFinale() {
        return EpreuveFinale;
    }

    public void setEpreuveFinale(String epreuveFinale) {
        EpreuveFinale = epreuveFinale;
    }

    public int getIdChampCompetence() {
        return IdChampCompetence;
    }

    public void setIdChampCompetence(int idChampCompetence) {
        IdChampCompetence = idChampCompetence;
    }

    public com.example.e1643891.tpmobile.DAL.ChampCompetence getChampCompetence() {
        return ChampCompetence;
    }

    public void setChampCompetence(com.example.e1643891.tpmobile.DAL.ChampCompetence champCompetence) {
        ChampCompetence = champCompetence;
    }
}
