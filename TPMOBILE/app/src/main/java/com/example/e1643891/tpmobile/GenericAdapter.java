package com.example.e1643891.tpmobile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.e1643891.tpmobile.DAL.Programme;

import java.util.List;



public class GenericAdapter extends ArrayAdapter<Programme> {
    private Context mContext;
    private List listElement;
    private Object currentElement;
    private TextView element;
    private View listItem;

    public GenericAdapter(Context context, List listElement) {
        super(context, 0, listElement);
        this.mContext = context;
        this.listElement = listElement;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        listItem = convertView;
        if (listItem == null) {
            listItem = LayoutInflater.from(mContext).inflate(R.layout.list_string, parent, false);
        }

        currentElement = this.listElement.get(position);
        element = listItem.findViewById(R.id.textView);
        element.setText(currentElement.toString());
        return listItem;
    }
}
