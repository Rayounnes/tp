package com.example.e1643891.tpmobile.API;

import android.content.Context;
import android.widget.ListView;
import com.example.e1643891.tpmobile.DAL.CoursListAdapter;
import com.example.e1643891.tpmobile.DAL.Cours;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListCoursApi extends MainApi {

    private Call<List<Cours>> call;
    private ListView liste;

    public ListCoursApi(Context context, ListView listview){

        super(context,listview);
        liste = listview;
    }

    public void DisplayCours(){
               Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);

        call = api.getCours();

        call.enqueue(new Callback<List<Cours>>(){
            @Override
            public void onResponse(Call<List<Cours>> call, Response<List<Cours>> response) {
                Display(response.body());
            }

            @Override
            public void onFailure(Call<List<Cours>> call, Throwable t) {}
        });
    }

    protected void Display(List body){
        listitem = body;
        CoursListAdapter adapter = new CoursListAdapter(context,0, listitem);
        listview.setAdapter(adapter);
    }
}
