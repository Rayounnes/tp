package com.example.e1643891.tpmobile.DAL;

/**
 * Created by mlevasseur on 2018-10-14.
 */

public class Savoir {

    private int Id;
    private String Description;
    private int Type;
    private int No;
    private int IdElementCompetence;
    private ElementCompetence ElementCompetence;

    public Savoir() {
    }

    public Savoir(int id, String description, int type, int no, int idElementCompetence, ElementCompetence elementCompetence) {
        Id = id;
        Description = description;
        Type = type;
        No = no;
        IdElementCompetence = idElementCompetence;
        ElementCompetence = elementCompetence;

    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String desc) {
        this.Description = desc;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        this.Type = type;
    }

    public int getNo() {
        return No;
    }

    public void setNo(int no) {
        this.No = no;
    }

    public int getIdElementCompetence() {
        return IdElementCompetence;
    }

    public void setIdElementCompetence(int id) {
        this.IdElementCompetence = id;
    }

    public ElementCompetence getElementCompetence() {
        return ElementCompetence;
    }

    public void setElementCompetence(ElementCompetence elem) {
        this.ElementCompetence = elem;
    }


    @Override
    public boolean equals(Object o) {

        boolean egale = false;

        if (o != null) {
            if (getClass() == o.getClass()) {

                Savoir s = (Savoir) o;
                egale = (Id == s.Id);
            }
        }
        return egale;
    }

    @Override
    public int hashCode() {
        int result = Id;
        result = 31 * result + (Description != null ? Description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return this.getNo() + "\n" + this.getDescription();
    }

    //Je ne connais pas assez bien les parcelable pour l'implanter, Tuan je te laisse le faire


}
