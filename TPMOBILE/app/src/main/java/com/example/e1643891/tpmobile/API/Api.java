package com.example.e1643891.tpmobile.API;

import com.example.e1643891.tpmobile.DAL.ChampCompetence;
import com.example.e1643891.tpmobile.DAL.Competence;
import com.example.e1643891.tpmobile.DAL.ContexteRealisation;
import com.example.e1643891.tpmobile.DAL.Cours;
import com.example.e1643891.tpmobile.DAL.CriterePerformance;
import com.example.e1643891.tpmobile.DAL.CriterePerformanceCompetence;
import com.example.e1643891.tpmobile.DAL.ElementCompetence;
import com.example.e1643891.tpmobile.DAL.Programme;
import com.example.e1643891.tpmobile.DAL.SavoirElemCompetence;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface Api {

    String BASE_URL = "http://gestionprogrammemobile.sv55.cmaisonneuve.qc.ca/";
    @GET("api/Programmes")
    Call<List<Programme>> getProgrammes();
    @PUT("api/Cours/{id}")
    Call<Cours> putCours(@Path("id") int id, @Body Cours cours);
    @GET("api/champsCompetencesProgrammeParIdCours/{id}")
    Call<List<ChampCompetence>> getChampsCompetencesParCours(@Path("id") int id);
    @GET("api/CompetencesParProgramme/{idprog}")
    Call<List<Competence>> getCompetences(@Path("idprog") int idprog);
    @GET("api/ElementCompetencesParIdCompetence/{idcomp}")
    Call<List<ElementCompetence>> getElement(@Path("idcomp") int idcomp);
    @GET("api/CriterePerformancesParIdElement/{idelem}")
    Call<List<CriterePerformance>> getCritere(@Path("idelem") int idelem);
    @GET("api/CriterePerformancesParIdCompetence/{idelem}")
    Call<List<CriterePerformanceCompetence>> getCriterePerfoCompetence(@Path("idelem") int idelem);
    @GET("api/ContexteRealisationsParIdCompetence/{idelem}")
    Call<List<ContexteRealisation>> getContexteRealisation(@Path("idelem") int idelem);
    @GET("api/SavoirParElementCompetence/{idelem}")
    Call<List<SavoirElemCompetence>> getSavoir(@Path("idelem") int idelem);
    @GET("api/Cours")
    Call<List<Cours>> getCours ();
    @GET("api/CoursparId/{id}")
    Call<List<Cours>> getCoursById (@Path("id") int id);

}
