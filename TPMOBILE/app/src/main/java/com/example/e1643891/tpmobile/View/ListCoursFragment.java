package com.example.e1643891.tpmobile.View;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.e1643891.tpmobile.API.ListCoursApi;
import com.example.e1643891.tpmobile.DAL.Cours;
import com.example.e1643891.tpmobile.R;

public class ListCoursFragment extends Fragment {

    private Context context;
    ListCoursApi cApi;


    public static ListCoursFragment newInstance() {

      return new ListCoursFragment();
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity().getApplicationContext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_cours,
                                    container, false);
        ListView list_cour = v.findViewById(R.id.Liste_Cours);

        cApi = new ListCoursApi(context,list_cour);
        cApi.DisplayCours();

        list_cour.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cours c = (Cours) cApi.getListItem().get(position);
                changeActivity(c);

            }
        });


        return v;
    }

    public void changeActivity(Cours c) {
        Intent myIntent = new Intent(this.getActivity(), CoursActivity.class);
        myIntent.putExtra("cours", c);
        startActivity(myIntent);
    }

}
