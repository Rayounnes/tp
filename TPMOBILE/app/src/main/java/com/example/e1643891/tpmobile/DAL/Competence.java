package com.example.e1643891.tpmobile.DAL;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by mlevasseur on 2018-10-14.
 */

public class Competence implements Serializable {
    @SerializedName("Id")
    private int id;
    @SerializedName("Code")
    private String code;
    @SerializedName("Enonce")
    private String enonce;
    @SerializedName("Obligatoire")
    private boolean obligatoire;
    @SerializedName("Processus")
    private boolean processus;
    @SerializedName("Clarification")
    private String clarification;
    private String place;
    private String epreuveFinale;
    private String materiel;
    private String commentaires;
    private int dureeEstimee;
    private boolean plusDUneSession;
    private Date dateModification;

    private int idChampCompetence;
    @SerializedName("ChampCompetence")
    private ChampCompetence champCompetence;

    private List<ProfilCompetence> profilsCompetences;
    private List<Cours> cours;

    public Competence() {}

    public Competence(int id) {
        this.id = id;
    }

    public Competence(int id, String code, String enonce, boolean obligatoire) {
        this.id = id;
        this.code = code;
        this.enonce = enonce;
        this.obligatoire = obligatoire;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEnonce() {
        return enonce;
    }

    public void setEnonce(String enonce) {
        this.enonce = enonce;
    }

    public boolean isObligatoire() {
        return obligatoire;
    }

    public void setObligatoire(boolean obligatoire) {
        this.obligatoire = obligatoire;
    }

    public boolean isProcessus() {
        return processus;
    }

    public void setProcessus(boolean processus) {
        this.processus = processus;
    }

    public String getClarification() {
        return clarification;
    }

    public void setClarification(String clarification) {
        this.clarification = clarification;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getEpreuveFinale() {
        return epreuveFinale;
    }

    public void setEpreuveFinale(String epreuveFinale) {
        this.epreuveFinale = epreuveFinale;
    }

    public String getMateriel() {
        return materiel;
    }

    public void setMateriel(String materiel) {
        this.materiel = materiel;
    }

    public String getCommentaires() {
        return commentaires;
    }

    public void setCommentaires(String commentaires) {
        this.commentaires = commentaires;
    }

    public int getDureeEstimee() {
        return dureeEstimee;
    }

    public void setDureeEstimee(int dureeEstimee) {
        this.dureeEstimee = dureeEstimee;
    }

    public boolean isPlusDUneSession() {
        return plusDUneSession;
    }

    public void setPlusDUneSession(boolean plusDUneSession) {
        this.plusDUneSession = plusDUneSession;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public int getIdChampCompetence() {
        return idChampCompetence;
    }

    public void setIdChampCompetence(int idChampCompetence) {
        this.idChampCompetence = idChampCompetence;
    }

    public ChampCompetence getChampCompetence() {
        return champCompetence;
    }

    public void setChampCompetence(ChampCompetence champCompetence) {
        this.champCompetence = champCompetence;
    }

    public Collection<ProfilCompetence> getProfilsCompetences() {
        return profilsCompetences;
    }

    public Collection<Cours> getCours() {
        return cours;
    }

    @Override
    public boolean equals(Object o) {

        boolean egale = false;

        if (o != null) {
            if (getClass() == o.getClass()) {

                Competence c = (Competence) o;
                egale = (id == c.id);
            }
        }
        return egale;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (enonce != null ? enonce.hashCode() : 0);
        return result;
    }
}
