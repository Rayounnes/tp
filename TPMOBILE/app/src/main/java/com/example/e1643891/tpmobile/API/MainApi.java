package com.example.e1643891.tpmobile.API;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.e1643891.tpmobile.R;

import java.util.List;



abstract public class  MainApi {

    public Context context;
    public ListView listview;
    public List listitem;

    public MainApi(Context context,ListView listview){
        this.context = context;
        this.listview = listview;
    }

    protected void Display(List body){
        listitem = body;
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.list_string, R.id.textView, body);
        listview.setAdapter(adapter);
    }
    public List getListItem(){
        return listitem;
    }


}
