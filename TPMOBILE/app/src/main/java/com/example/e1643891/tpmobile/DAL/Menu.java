package com.example.e1643891.tpmobile.DAL;

import java.util.ArrayList;
import java.util.List;

public class Menu {

    public static List<String> GetElementMenu(){
        List data = new ArrayList();
        data.add("Analyse Champ Competence");
        data.add("Competence");
        data.add("Profils de sortie");
        data.add("Grille de cours");
        data.add("Logigramme de competence");
        data.add("Activite d'apprentissage");
        return data;
    }

}
